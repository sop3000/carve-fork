

This is a fork of the Carve CSG library from https://code.google.com/p/carve/
The code has been minimally adjusted to be able to build it with MS Visual Studio 2014.
The"intersect" utility has been extended to support array of trasformation 
matrices in the input file - needed to interface with the StructureSynth output.

	- sop3000@sopwerk.com, 12.08.15


How to use "intersect" utility:
-------------------------------

1. Create structure in Structure Synch. 
   Example:

		set maxdepth 400
		set maxobjects 1000
		 
		spiral
		 
		rule spiral w 100 {
			box
			{ y 1.0 rx 90 s 0.999 } spiral
		}
		 
		rule spiral w 100 {
			box
			{ y 1.0 rx 90 rz -90 s 0.999 } spiral
		}
		 
		rule spiral w 100 {
			box
			{ y 1.0 rx 90 rz 90 s 0.999 } spiral
		}
		 
		rule spiral w 80 {
			{ rx 90 s 1 0.2 1 } spiral
			{ ry 180 s 1 1.8 1 } spiral
		}


2. Press F6 to generate file of matrices in Structure Synch, using following "carve" template:

		<template defaultExtension="Structure Synth Object (*.carve)" name="CarverTemplate">
		 <description>&#xd;
		 This is an exporter that can be used together with the Carver intersect tool.&#xd;
		 </description>
		 <primitive name="begin"><![CDATA[]]></primitive>
		 <primitive name="end"><![CDATA[]]></primitive>
		 <primitive name="mesh"><![CDATA[]]></primitive>
		 <primitive name="box"><![CDATA[{matrix}
		]]></primitive>
		 <primitive name="sphere"><![CDATA[]]></primitive>
		</template>

3. Process matrices with Carve and produce OBJ file

	intersect -O -m -f sample.carve > sample.obj

4. Import OBJ file into Blender

